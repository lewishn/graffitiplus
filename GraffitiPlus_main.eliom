[%%shared
    open Eliom_content.Html5
    open D
    open Lwt
    open GraffitiPlus_draw
    open GraffitiPlus_components
    type messages =
        ((int * int * int) * string * float * int * (int * int) * (int * int))
        [@@deriving json]
]


let bus = Eliom_bus.create [%derive.json: messages]
let bus2 = Eliom_bus.create [%derive.json: messages]

let color_container = div ~a:[a_class ["col-md-6"; "col-sm-12"]] []

let page () =
    (html
        (Eliom_tools.F.head ~title:"Graffiti+"
            ~css:[["css";"GraffitiPlus.css"];["css";"bootstrap.min.css"]]
            ~js:[] ())
        (body [
            div ~a:[a_class ["container"]] [
                div ~a:[a_class ["navbar";"navbar-default"]] [h1 [pcdata "Graffiti+"]];
                div ~a:[a_class ["row"; "canvas_container"]] [
                    div ~a:[a_class ["col-md-12"]] [
                        canvas_elt];
                    div ~a:[a_class ["col-md-6"; "col-sm-12"]] [
                        div [label [pcdata "Brush Size: "]; size_slider];
                        div [label [pcdata "Brush Transparency: "]; transparency_slider];
                        div ~a:[a_class ["form-group"]] [
                            label [pcdata "Brush: "];
                            select_brush];
                        div [canvas_elt2]];

                    color_container]]]))

let%client init_client () =
    let canvas = To_dom.of_canvas ~%canvas_elt in
    let ctx = canvas##(getContext (Dom_html._2d_)) in
    let canvas2 = To_dom.of_canvas ~%canvas_elt2 in
    let ctx2 = canvas2##(getContext (Dom_html._2d_)) in

    let x = ref 0 and y = ref 0 in

    let colorpicker = Ow_color_picker.create ~width:200 () in
    let elt = Eliom_content.Html5.To_dom.of_div ~%color_container in
    Ow_color_picker.append_at (elt) colorpicker;
    Ow_color_picker.init_handler colorpicker;

    let set_coord ev cvs =
        let x0, y0 = Dom_html.elementClientPosition cvs in
        x := ev##.clientX - x0; y := ev##.clientY - y0
    in

    let compute_line ev cvs =
        let oldx = !x and oldy = !y in
        set_coord ev cvs;

        let rgb = Ow_color_picker.get_rgb colorpicker in

        let brush_size_slider = Eliom_content.Html5.To_dom.of_input ~%size_slider in
        let size = int_of_string (Js.to_string brush_size_slider##.value) in

        let brush_transparency_slider = Eliom_content.Html5.To_dom.of_input ~%transparency_slider in
        let transparency = float_of_string (Js.to_string brush_transparency_slider##.value) in

        let brush_options_elt = Eliom_content.Html5.To_dom.of_select ~%select_brush in
        let brush = Js.to_string brush_options_elt##.value in

        (rgb, brush, transparency, size, (oldx, oldy), (!x, !y))
    in

    let line cvs cntx bs ev =
        let v = compute_line ev cvs in
        let _ = Eliom_bus.write bs v in
        draw cntx v;
        Lwt.return ()
    in


    Lwt.async (fun () ->
        let open Lwt_js_events in
        mousedowns canvas (fun ev _ ->
            set_coord ev canvas; line canvas ctx ~%bus ev >>= fun () ->
            Lwt.pick
                [mousemoves Dom_html.document (fun x _ -> line canvas ctx ~%bus x);
                mouseup Dom_html.document >>= line canvas ctx ~%bus]));
    Lwt.async (fun () ->
        let open Lwt_js_events in
        mousedowns canvas2 (fun ev _ ->
            set_coord ev canvas2; line canvas2 ctx2 ~%bus2 ev >>= fun () ->
            Lwt.pick
                [mousemoves Dom_html.document (fun x _ -> line canvas2 ctx2 ~%bus2 x);
                mouseup Dom_html.document >>= line canvas2 ctx2 ~%bus2]));
    Lwt.async (fun () -> Lwt_stream.iter (draw ctx) (Eliom_bus.stream ~%bus));
    Lwt.async (fun () -> Lwt_stream.iter (draw ctx2) (Eliom_bus.stream ~%bus2))
