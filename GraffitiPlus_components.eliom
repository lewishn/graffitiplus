[%%shared
    open Eliom_content.Html5.D
]

let%shared width = 800
let%shared height = 500
let%shared width2 = 100
let%shared height2 = 100

let canvas_elt =
    canvas ~a:[a_width width; a_height height]
        [pcdata "your browser doesn't support canvas"]

let canvas_elt2 =
    canvas ~a:[a_width width2; a_height height2; a_id "brush"]
        [pcdata "your browser doesn't support canvas"]

let size_slider =
    Form.input
        ~a:[a_id "slider"; a_input_min 1.; a_input_max 80.]
        ~input_type:`Range
        Form.int

let transparency_slider =
    Form.input
        ~a:[a_id "slider"; a_input_min 0.; a_input_max 100.]
        ~input_type:`Range
        Form.float

let select_brush =
    Raw.select ~a:[a_class ["form-control"]] [
        option ~a:[a_value "round"; a_selected `Selected] (pcdata "Line");
        option ~a:[a_value "blur"] (pcdata "Blurred");
        option ~a:[a_value "blotch"] (pcdata "Blotch");
        option ~a:[a_value "star"] (pcdata "Star");
        option ~a:[a_value "spray"] (pcdata "Spray");
        option ~a:[a_value "custom"] (pcdata "Custom Brush");
        option ~a:[a_value "erase"] (pcdata "Eraser")
    ]