[%%shared
    open Eliom_content.Html5
    open GraffitiPlus_components
]
let%client calc_xy (x1, y1) (x2, y2) = (x1 + x2) / 2, (y1 + y2) / 2

let%client draw_custom ctx ((r, g, b, a), size, (x1, y1), (x2, y2)) =
    let x, y = calc_xy (x1, y1) (x2, y2) in
    let x_middle = x - width2 / 2 in
    let y_middle = y - height2 / 2 in
    let cvs = To_dom.of_canvas ~%canvas_elt2 in
    ctx##(drawImage_fromCanvas cvs (float x_middle) (float y_middle))


let%client draw_line ctx ((r, g, b, a), size, (x1, y1), (x2, y2)) erase =
    let color = CSS.Color.string_of_t (CSS.Color.rgb ~a:a r g b) in
    ctx##save;
    ctx##.lineCap := Js.string "round";
    ctx##.strokeStyle := (Js.string color);
    ctx##.lineWidth := float size;
    begin
        if erase then
            ctx##.globalCompositeOperation := Js.string "destination-out"
    end;
    ctx##beginPath;
    ctx##(moveTo (float x1) (float y1));
    ctx##(lineTo (float x2) (float y2));
    ctx##stroke;
    ctx##restore

let%client draw_smooth ctx ((r, g, b, a), size, (x1, y1), (x2, y2)) =
    let color = CSS.Color.string_of_t (CSS.Color.rgb ~a:a r g b) in
    ctx##save;
    ctx##.lineCap := Js.string "round";
    ctx##.strokeStyle := (Js.string color);
    ctx##.lineWidth := float size;
    ctx##.shadowBlur := float 10;
    ctx##.shadowColor := Js.string color;
    ctx##beginPath;
    ctx##(moveTo (float x1) (float y1));
    ctx##(lineTo (float x2) (float y2));
    ctx##stroke;
    ctx##restore

let%client draw_star ctx ((r, g, b, a), size, (x1, y1), (x2, y2)) =
    let x, y = calc_xy (x1, y1) (x2, y2) in
    let color = CSS.Color.string_of_t (CSS.Color.rgb ~a:a r g b) in
    ctx##save;
    ctx##.strokeStyle := (Js.string color);
    ctx##(translate (float x) (float y));
    ctx##beginPath;
    ctx##(rotate 0.31415926535);
    for i = 1 to 5 do
        ctx##(lineTo (float 0) (float size));
        ctx##(translate (float 0) (float size));
        ctx##(rotate 0.62831853071);
        ctx##(lineTo (float 0) (float (-size)));
        ctx##(translate (float 0) (float (-size)));
        ctx##(rotate (1.88495559215))
    done;
    ctx##(lineTo (float 0) (float size));
    ctx##closePath;
    ctx##stroke;
    ctx##restore

let%client draw_spray ctx ((r, g, b), size, (x1, y1), (x2, y2)) =
    let random () =
        let r = Js.math##random *. float_of_int (2 * size + 1) in
        r -. (float_of_int size)
    in
    let x, y = calc_xy (x1, y1) (x2, y2) in
    let color = CSS.Color.string_of_t (CSS.Color.rgb r g b) in
    ctx##save;
    ctx##.fillStyle := (Js.string color);
    for i = 1 to (size * 3) / 4 do
        let offsetX = (float_of_int x) +. (random ()) in
        let offsetY = (float_of_int y) +. (random ()) in
        ctx##(fillRect offsetX offsetY 1. 1.)
    done;
    ctx##restore

let%client draw_blotch ctx ((r, g, b, a), size, (x1, y1), (x2, y2)) =
    let x, y = calc_xy (x1, y1) (x2, y2) in
    let color = CSS.Color.string_of_t (CSS.Color.rgb r g b) in
    ctx##save;
    ctx##.fillStyle := (Js.string color);
    ctx##beginPath;
    ctx##.globalAlpha := a;
    let radius = Js.math##random *. (float size) in
    ctx##(arc (float x) (float y) radius 0. 6.28318530718 Js._false);
    ctx##fill;
    ctx##restore

let%client draw ctx ((r, g, b), brush, transparency, size, (x1, y1), (x2, y2)) =
    let transp = transparency /. 100. in
    if brush = "star" then
        draw_star ctx ((r, g, b, transp), size, (x1, y1), (x2, y2))
    else if brush = "spray" then
        draw_spray ctx ((r, g, b), size, (x1, y1), (x2, y2))
    else if brush = "blur" then
        draw_smooth ctx ((r, g, b, transp), size, (x1, y1), (x2, y2))
    else if brush = "blotch" then
        draw_blotch ctx ((r, g, b, transp), size, (x1, y1), (x2, y2))
    else if brush = "erase" then
        draw_line ctx ((0, 0, 0, 1.), size, (x1, y1), (x2, y2)) true
    else if brush = "custom" then
        draw_custom ctx ((255, 255, 255, 1.), size, (x1, y1), (x2, y2))
    else
        draw_line ctx ((r, g, b, transp), size, (x1, y1), (x2, y2)) false
