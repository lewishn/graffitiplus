[%%shared
    open Eliom_lib
    open Eliom_content
    open Html5.D
    open GraffitiPlus_main
]


module GraffitiPlus =
    Eliom_registration.App (
        struct
            let application_name = "GraffitiPlus"
        end)

let main_service =
    GraffitiPlus.register_service
        ~path:[""]
        ~get_params:Eliom_parameter.unit
        (fun () () ->
            let _ = [%client (init_client () : unit) ] in
            Lwt.return (page ()))

